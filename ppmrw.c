/*
 * Peter Haschke
 * Project 1 - Images
 * 9/15/16
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h> //Import for use of isspace().

typedef struct pixelStruct{
        unsigned char r, g, b;
} Pixel;


//Function Prototypes
int getNumber(FILE* fdIn, char* buffer);
int getBuffer(FILE* fdIn, FILE* fdOut, char* buffer);
int getComments(FILE* fdIn, FILE* fdOut);
int readAscii(FILE* fdIn, int numPixels, Pixel *pixelBuffer);
int readBinary(FILE* fdIn, int numPixels, Pixel *pixelBuffer);
int writeAscii(FILE* fdOut, int numPixels, Pixel *pixelBuffer);
int writeBinary(FILE* fdOut, int numPixels, Pixel *pixelBuffer);

//Proper usage ppmrw 3 input.ppm output.ppm
int main(int argc, char *argv[]){
        if (argc != 4){
                fprintf(stderr, "Error: Example usage is: ppmrw 3 input.ppm output.ppm\n");
                return(-1);
        }
        
        int outputType;
        if(atoi(argv[1]) == 3){
                outputType = 3;
        }else if((atoi(argv[1]) == 6)){
                outputType = 6;
        }else{
                fprintf(stderr, "Error: Supported file types are 3 or 6.\n");
                return(-1);

        }

        FILE* fdIn;
        //Open existing file for reading.
        fdIn = fopen(argv[2], "r");
        if(fdIn == NULL){
                fprintf(stderr, "Error: in opening the file for reading\n");
                return(-1);
        }

        FILE* fdOut;
        //Open and create new output file.
        fdOut = fopen(argv[3], "w");
        if(fdOut == NULL){
                fprintf(stderr, "Error: in  opening the file for writing\n");
                return(-1);
        }

        //Create a buffer to read in parts of the header.
        char* buffer;
        buffer = (char*)malloc(32);

        //Write magic number of new output file.
        fprintf(fdOut, "%c", 'P');
        fprintf(fdOut, "%d", atoi(argv[1]));

        //Get the first value of the magic number of the input file and verify it is a "P".
        if(fgetc(fdIn) != 'P'){
                fprintf(stderr, "Error: Not a Supported File Type\n");
                return(1);
        }
        
        int inputType;
        
        //Get the second part fo the magic number from the input file and verify it is a 3 or a 6.
        //Save value as a int in a variable. Error out if it's not of either type.
        char c = fgetc(fdIn);
        if(c == '3'){
                inputType = 3;
        }else if(c == '6'){
                inputType = 6;
        }else {
                fprintf(stderr, "Error: Not a Supported File Type\n");
           
        }
        //Print out character after the magic number.
        fprintf(fdOut, "%c", fgetc(fdIn));

        //Call getBuffer fill the buffer with the first non comment value which is the width.
        getBuffer(fdIn, fdOut, buffer);

        //Get first returned value which is the width and conver it to an int to store.
        int width = atoi(buffer);

        //Get next non comment value which is the height.
        getBuffer(fdIn, fdOut, buffer);
        int height = atoi(buffer);

        //Get next non comment value which is the magic number.
        getBuffer(fdIn, fdOut, buffer);
        int maxNum = atoi(buffer);
        
        //Throw and error if the file uses 2 byte numbers instead of just 1.
        if(maxNum > 255){
                fprintf(stderr, "Error: Program only support 1 byte images");
        }
        
        //Calculate the total number of pixels in the image.
	int numPixels = width*height;

	//Set up pixel stuct buffer.
	Pixel *pixelBuffer;
    	pixelBuffer = (Pixel *)malloc(sizeof(Pixel)*numPixels);

	

        //Call proper functions to read in data into Pixel buffer and write it out.
        if(inputType == 3){
                readAscii(fdIn, numPixels, pixelBuffer);
        }else{
                readBinary(fdIn, numPixels, pixelBuffer);
        }
        //Close input file as it is not needed anymore.
        fclose(fdIn);

        if(outputType == 3){
                writeAscii(fdOut, numPixels, pixelBuffer);
        }else{
                writeBinary(fdOut, numPixels, pixelBuffer);
        }
        //Close output file.
        fclose(fdOut);


}

//Function to read ascii characters in and store in the pixelBuffer as unsigned chars.
int readAscii(FILE* fdIn, int numPixels, Pixel *pixelBuffer){
	int i, pixelCount;
	char* numBuffer = (char*)malloc(32);
	for(i = 0; i < numPixels; i++){
		if(getNumber(fdIn, numBuffer) != 0){
			fprintf(stderr, "Error: Hit end of file before all pixels were read.");
		}
		pixelBuffer->r = (unsigned char)atoi(numBuffer);
		if(getNumber(fdIn, numBuffer) != 0){
			fprintf(stderr, "Error: Hit end of file before all pixels were read.");
		}
		pixelBuffer->g = (unsigned char)atoi(numBuffer);
		if(getNumber(fdIn, numBuffer) != 0){
			if(pixelCount != numPixels-1){	
			fprintf(stderr, "Error: Hit end of file before all pixels were read.");
			}
		}
		pixelBuffer->b = (unsigned char)atoi(numBuffer);
		pixelCount++;
		*pixelBuffer++;

	}
        return(0);
}

//Function to read binary from the input file and store it in the pixelBuffer.
int readBinary(FILE* fdIn, int numPixels, Pixel *pixelBuffer){
        int i, pixelCount;
        size_t bytesRead;
        for(i = 0; i < numPixels; i++){
                fread(&pixelBuffer->r, 1, 1, fdIn);
                fread(&pixelBuffer->g, 1, 1, fdIn);
                fread(&pixelBuffer->b, 1, 1, fdIn);
                
                *pixelBuffer++;
                pixelCount++;
        }
        return(0);
}

//Function to convert and write ascii from the picelBuffer to a output file.
int writeAscii(FILE* fdOut, int numPixels, Pixel *pixelBuffer){
        int i, pixelCount;
        for(i = 0; i < numPixels-1; i++){
                fprintf(fdOut, "%d%c%d%c%d%c", pixelBuffer->r, 10, pixelBuffer->g, 10, pixelBuffer->b, 10);
                *pixelBuffer++;
        }
        //Add the last pixel print to avoid ending up with an extra space at the end of the file.
        fprintf(fdOut, "%d%c%d%c%d", pixelBuffer->r, 10, pixelBuffer->g, 10, pixelBuffer->b);

        return(0);
}

//Function to write binary from pixelBuffer to a output file.
int writeBinary(FILE* fdOut, int numPixels, Pixel *pixelBuffer){
        int i, pixelCount;
        size_t bytesWrote;
        for(i = 0; i < numPixels; i++){
                fwrite(&pixelBuffer->r, 1, 1, fdOut);
                fwrite(&pixelBuffer->g, 1, 1, fdOut);
                fwrite(&pixelBuffer->b, 1, 1, fdOut);
                
                *pixelBuffer++;
                pixelCount++;
        }
        return(0);
}

//Get number function fills a buffer with the ascii from a single number from the file.
int getNumber(FILE* fdIn, char* buffer){
	char c;
	while((c = fgetc(fdIn)) != EOF){
		if(isspace(c)){
			*buffer = '\0';
                        return(0);
		}else{
			*buffer = c;
			*buffer++;
		}
	}
	return(-1);
}
//Function to fill a buffer with characters from the header and call the getComments functions to handle any comments.
int getBuffer(FILE* fdIn, FILE* fdOut, char* buffer){
        char c;
        while((c = fgetc(fdIn)) != EOF){
                if(isspace(c)){
                //A space means we have something to be returned in out buffer.
                //Null terminate buffer and return.
                        fprintf(fdOut, "%c", c);
                        *buffer = '\0';

                        return(0);
                
                }else if(c == '#'){
                //If we read a # we know we have a comment, use comment reading function to get whole comment.
                        fprintf(fdOut, "%c", c);
                        getComments(fdIn, fdOut);


                }else{
                //If not whitespace or start of comment read the byte into out buffer.
                        *buffer = c;
                        *buffer++;
                        fprintf(fdOut, "%c", c);
                }
        }
        return(-1);
}

//Function to take in a fdIn thats at the start of a comment and read it into a file until it hits a newline character.
int getComments(FILE* fdIn, FILE* fdOut){
        char c;
        while((c = fgetc(fdIn)) != EOF){

                fprintf(fdOut, "%c", c);

                if(c == '\n' || c == '\r'){
                        break;
                }
        }
        return(0);
}

